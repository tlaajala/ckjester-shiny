Hey all,

while everybody's busy desperately fighting for their life (and I am also), I'd also like to advertise a little side-kick project I've been working with the codename [i][b]CKJester[/b][/i]. Basically I'm hoping to offer some CK API-based service (planning free of charge with voluntary donation basis) to enthusiastic CK'ers who'd like some systematic visualizations and possibly statistical modeling of CK.

So far:
- Made a R script that calls the API on regular intervals with a set delay in-betwen each item call to disperse the burden, obtaining current buy/sell offer status of interesting items.
- Wraps the retrieved JSON to a R-friendly data format and combines the longitudinal information which is collected as described above.
- Made a few plot functions (i.e. buy/sell offer quantiles weighted by the amount of the particular item being sold, total sums of buy/sell etc)
- I plan to implement a graphical user-interface that accesses this processed data, where user may customize plots and perform interesting stuff of their preference (I might be able to put it up for free using R Shiny, but this remains to be seen). 
(R is a free flexible statistical software/language available at www.R-project.org for anybody that's interested)

So I did my first experiments on the above pipeline last weekend and have approximately 5 days amount of data with varying intervals, and I'd like to present you examples of what kind of visualizations could be produced.



I know you guys and gals love [b]CAN[/b] so I started with that, and it's also very topical due to its connection to the health challenge(s). Basically here's a plotting of how CAN offers have changed during the past few days:

[img]http://i.imgur.com/zkDSuE0.png[/img]

It's a work in progress so it's not very polished, but here you can already see a few interesting things:
- There was a drastic reduction in CAN buy/sell offers in-between Dec 1st/2nd EET night; possibly people waking up to the fact that there's yet an another health challenge to tackle :)
- Feasible off-the-shelf prices of CAN seem to have been a little tip over 10k during the past week. Notice it's log-transformed y-axis though, to compensate for a few outlierish sell/buy offers.



And then an another example; people got excited about silver recently! So I took a snapshot of a silver coin CS1606E:

[img]http://i.imgur.com/S6xZ0Bb.png[/img]

- I think it was around 30th of November when people started realizing that silver was probably going to be worth more in the future, somebody buying off half of this particular coin. Sadly I'm missing the middle-point of that eventful night as I was unable to access the API for some unknown reason.
- With a delay of few days, people (or a person) have set a few new buy offers for newly discovered silver. Seems that nobody's willing to part of their precious CS1606E:s yet though, and there's quite a gap between the sell and buy offers.



I'm hoping these plots to have more informativeness when this has been gathered for a longer period of time. I hope you find these interesting, it's just a concept example of what I'm doing on my spare time. These default plots could of course be customized in the GUI to answer more specific questions. I think the API is a very interesting asset in CK, and at least from my experience so far, retrieving these sell/buy orders is not a very computationally intensive task. Only thing I'm currently missing is an API functionality that would allow inspecting the transaction log ;)

Stay tuned!
