library(shiny)
# Load current database for CK
# Remember to change this both in ui.R and server.R
#dbname <- "DB_2015-12-18"
# LITE version
# CASE SENSITIVE ALSO IN THE FILE FORMATS!!
#dbname <- "DB_2016-01-21_LITE"
#dbfull <- "DB_2016-01-10"
# Load the FULL database
#load(paste(dbfull, ".RData", sep=""))
# Load the LITE database
load("DB_LITE_Agora_Latest_Automated.RData")
version <- "v0.4"
versionname <- "New Start"
# First time I collected info
# Make sure time variables are globally visible
#mintime <<- as.POSIXct("2015-11-29 01:32:53")
mintime <<- as.Date(as.POSIXct("2015-11-29 01:32:53"), format="Y-M-D")
maxtime <<- as.Date(as.POSIXct(maxtime), format="Y-M-D")

#itemIDs <- unique(db[,"itemID"])
# LITE update
itemIDs <- names(items)
#print(length(itemIDs))

# Define UI for application that draws a histogram
shinyUI(
	#fluidPage(
	# Application title
	navbarPage( 
		position="static-top",
		#title = p(img(src="jester.png", width=25), "CKJester v0.1 alpha"),
		title = paste("CKJester alpha", version, ":", versionname),
		#title = fixedPanel(top = 0, left = 0, right = 0, bottom = 0, width=100, height=100, p(img(src="jester.png", width=50), "CKJester v0.1 alpha", h5(paste("Database version")))),
		#collapsible = TRUE,
		#fluid = FALSE,
		#icon = "jester.png",
		#icon = img(src="jester.png", width=25),
		
	#navbarMenu(
	#	title = "plot1",
	#	icon = img(src="jester.png", width=50),
	#	#title = p(img(src="jester.png", width=50), "CKJester v0.1 alpha", h5(paste("Database version"))),
	# Title
	#titlePanel(p(img(src="jester.png", width=50), "CKJester v0.1 alpha", h5(paste("Database version")))),
	# Available tabs
	#tabsetPanel(id = "tabSelected",
	#	tabPanel("Plot1", uiOutput("Plot1")),
	#	tabPanel("Plot2", uiOutput("Plot2"))
	#),

	# Sidebar with a slider input for the number of bins
	#	sidebarLayout(
	#	sidebarPanel(
	tabPanel("About",
		sidebarPanel(
			p(img(src="jester.png", width=140))#,
			#p("Currently utilized database version:", textOutput("maxtime"))
		),
		mainPanel(
			p(h4("Disclaimer"), 
				"CKJester is provided as-is, with no liability for its correctness or functionality. CKJester is in no way affiliated with cryptokingdom.me, other than that it calls CK's API and gathers the data here."
			),
			p(h4("Author"), 
				"CKJester is implemented by Syksy (character ID 138 in CK, Syksy @ freenode). Any in-game donations are very much welcome, should you find CKJester useful and/or appreciate the effort put into it."
			),
			p(h4("Version history"), 
				strong("New Start \n- Sep 10, 2016; v0.4 "), "- Additional features prepared; time frame choice for offers available, lot database under construction etc.",
				br(),
				strong("Coronation and Automation \n- Jun 12, 2016; v0.3 "), "- Whole pipeline is now automated to extract and process a DB directly from the API without manual intervention",
				br(),
				strong("Raffle Mania\n- Jan 14, 2016; v0.2.2 "), "- Bugfixes and a light version (pre-processed database) of the My Offers -funtions",
				br(),
				strong("Cannons and Counties\n- Jan 11, 2016; v0.2.1 "), "- Fixed a couple bugs in Offers Lite, added My Offers functionality, abbreviation \'mil\' can be used to insert 1e6",
				br(),
				strong("Unnamed\n- Jan 10, 2016; v0.2 "), "- First lite version with precomputed statistics, that does not choke on busy items but at the expense of flexibility options",
				br(),
				strong("Unnamed\n- Dec 20, 2015; v0.1.2 "), "- First deployment on shinyapps.io, improvements to the Offers-plot, output possible as PDF",
				br(),
				strong("Unnamed\n- Dec 17, 2015; v0.1.1 "), "- Updated offer plot with polygons and additional customizations. First attempt to deploy on shinyapps.io-platform.",
				br(),
				strong("Unnamed\n- Dec 4, 2015; v0.1 "), "- First offer plot and a draft of the R Shiny GUI",
				br()
			)#, Commenting out planned features, not really keeping up and/or necessary
			#p(h4("Planned features (priority)"),
			#	#"- Text and font adjustments in the browser-based plots (high)",
			#	#br(),
			#	#"- Advanced features and customization to the Offers-plot (high)",
			#	#br(),
			#	#"- Omit suspected outliers in offers-plot (high)",
			#	#br(),
			#	#"- Automated database upkeep (medium)",
			#	#br(),
			#	#"- User friendliness, tidying up the GUI (medium)",
			#	#br(),
			#	#"- Less reactive plots that only change after a change has been confirmed (low)",
			#	#br(),
			#	#"- Transaction-log analysis (high, but depends on API functionality)",
			#	#br(),
			#	"- Make the GUI and functionality lighter and faster (high)",
			#	br(),
			#	"- New features when e.g. transaction log API becomes available (?)",
			#	br(),				
			#	"- Requests (?)",
			#	br()
			#)
		
		)
	),
	tabPanel("Offers Lite",
		sidebarPanel(
		# Copy the line below to make a date range selector
		#dateRangeInput("dates", label = h3("Date range")),
				
                #sliderInput("slider1", label = h3("Sliders"), min = 0, max = 100, value = 50),
                #selectInput("item", label = h4("itemID choice list"), choices = itemIDs, selected = which(itemIDs=="CAN")),
                selectInput("item", label = h4("itemID choice list"), choices = itemIDs, selected = "CAN"),
             	textInput("item2", label = h4("itemID (text, overrides choice menu)"), value = ""),
                
                #
                #radioButtons("itemradio", label = h3("itemID (radiobutton input)"), choices = list("Choice 1" = 1, "Choice 2" = 2, "Choice 3" = 3), selected = 0),
                #numericInput("num", label = h3("Numeric input"), value = 1),
                ### ADD DATE RANGE LATER
                #dateRangeInput("daterange", "Date range:", start = strsplit(min(db[,"Time"]), " ")[[1]][1], end = strsplit(max(db[,"Time"]), " ")[[1]][1]),
                #sliderInput("quantileprecision", label="Precision in weighted quantiles", min=0.01, max=0.5, value=0.1, round=FALSE, ticks=TRUE, sep=TRUE),
                textInput("minoffer", label="Minimum offer value", value=""),
                textInput("maxoffer", label="Maximum offer value", value=""),
                dateInput("datemin", label="Start date", value = as.character(mintime)),
                dateInput("datemax", label="End date", value = as.character(maxtime)),
                #checkboxInput("polygon", label = "Colored polygon quantiles", value = TRUE),
                #checkboxInput("scatteroffers", label = "Scatterplot of actual offers", value = FALSE),
                checkboxInput("logy1", label = "Log-transformed y-axis (offers)", value = TRUE),
                #checkboxInput("logy2", label = "Log-transformed y-axis (amounts)", value = FALSE),
                checkboxInput("meancurve", label = "Mean curve lowest sell / highest buy", value = TRUE),
                checkboxInput("lineticks", label = "Horizontal line ticks", value = FALSE),
		
		#selectInput("colorpalettesell", label = h4("Sell offers color palette"), choices = 
		#	c(
		#		"Main: Green; Gradient: Green-Yellow",
		#		"Main: Orange; Gradient: Orange-Yellow-Olivedrab"
		#	), 
		#	selected = "Main: Green; Gradient: Green-Yellow"
		#),
		#selectInput("colorpalettebuy", label = h4("Buy offers color palette"), choices = 
		#	c(
		#		"Main: Blue; Gradient: Blue-Cyan", 
		#		"Main: Purple; Gradient: Purple-Red-Pink"
		#	), 
		#	selected = "Main: Blue; Gradient: Blue-Cyan"
		#),
                
		checkboxInput('returnpdf', 'Download figure as PDF', FALSE),
		conditionalPanel(
		    condition = "input.returnpdf == true",
		    strong("PDF size (inches):"),
		    sliderInput(inputId="w", label = "width:", min=3, max=20, value=8, width=100, ticks=F),
		    sliderInput(inputId="h", label = "height:", min=3, max=20, value=6, width=100, ticks=F),
		    br(),
		    downloadLink('pdflink')
		)
                
                ), # End of sidebarPanel
		#sliderInput("bins",
		#	  "Number of bins:",
		#	  min = 1,
		#	  max = 50,
		#	  value = 30)
		#),
		# Show a plot of the generated distribution
		mainPanel(
			plotOutput("offersLitePlot")
		)
	),	
	### OLD OFFERS PLOT
	#
	#tabPanel("Offers",
	#	sidebarPanel(
	#	# Copy the line below to make a date range selector
	#	#dateRangeInput("dates", label = h3("Date range")),
	#			
        #        #sliderInput("slider1", label = h3("Sliders"), min = 0, max = 100, value = 50),
        #        #selectInput("item", label = h4("itemID choice list"), choices = itemIDs, selected = which(itemIDs=="CAN")),
        #        selectInput("item", label = h4("itemID choice list"), choices = itemIDs, selected = "CAN"),
        #     	textInput("item2", label = h4("itemID (text, overrides choice menu)"), value = ""),
        #        
        #        #
        #        #radioButtons("itemradio", label = h3("itemID (radiobutton input)"), choices = list("Choice 1" = 1, "Choice 2" = 2, "Choice 3" = 3), selected = 0),
        #        #numericInput("num", label = h3("Numeric input"), value = 1),
        #        dateRangeInput("daterange", "Date range:", start = strsplit(min(db[,"Time"]), " ")[[1]][1], end = strsplit(max(db[,"Time"]), " ")[[1]][1]),
        #        sliderInput("quantileprecision", label="Precision in weighted quantiles", min=0.01, max=0.5, value=0.1, round=FALSE, ticks=TRUE, sep=TRUE),
        #        textInput("minoffer", label="Minimum offer value", value=""),
        #        textInput("maxoffer", label="Maximum offer value", value=""),
        #        checkboxInput("polygon", label = "Colored polygon quantiles", value = TRUE),
        #        checkboxInput("scatteroffers", label = "Scatterplot of actual offers", value = FALSE),
        #        checkboxInput("logy1", label = "Log-transformed y-axis (offers)", value = TRUE),
        #        checkboxInput("logy2", label = "Log-transformed y-axis (amounts)", value = FALSE),
        #        checkboxInput("meancurve", label = "Mean curve lowest sell / highest buy", value = TRUE),
        #        checkboxInput("lineticks", label = "Horizontal line ticks", value = FALSE),
	#	
	#	selectInput("colorpalettesell", label = h4("Sell offers color palette"), choices = 
	#		c(
	#			"Main: Green; Gradient: Green-Yellow",
	#			"Main: Orange; Gradient: Orange-Yellow-Olivedrab"
	#		), 
	#		selected = "Main: Green; Gradient: Green-Yellow"
	#	),
	#	selectInput("colorpalettebuy", label = h4("Buy offers color palette"), choices = 
	#		c(
	#			"Main: Blue; Gradient: Blue-Cyan", 
	#			"Main: Purple; Gradient: Purple-Red-Pink"
	#		), 
	#		selected = "Main: Blue; Gradient: Blue-Cyan"
	#	),
        #        
	#	checkboxInput('returnpdf', 'Download figure as PDF', FALSE),
	#	conditionalPanel(
	#	    condition = "input.returnpdf == true",
	#	    strong("PDF size (inches):"),
	#	    sliderInput(inputId="w", label = "width:", min=3, max=20, value=8, width=100, ticks=F),
	#	    sliderInput(inputId="h", label = "height:", min=3, max=20, value=6, width=100, ticks=F),
	#	    br(),
	#	    downloadLink('pdflink')
	#	)
        #        
        #        ), # End of sidebarPanel
	#	#sliderInput("bins",
	#	#	  "Number of bins:",
	#	#	  min = 1,
	#	#	  max = 50,
	#	#	  value = 30)
	#	#),
	#	# Show a plot of the generated distribution
	#	mainPanel(
	#		plotOutput("offersPlot")
	#	)
	#),
	#tabPanel("My Offers",
	#	p("foo to be added.")
	#),
	#tabPanel("bar",
	#	p("bar to be added.")
	#)#,
	navbarMenu("My Offers",
		tabPanel("Latest Offers",
			sidebarPanel(
				#div("Lite version is under development, this functionality is very slow atm", style = "color:red"),
				textInput("char1", label = h4("Character shortName"), value = "")
			),
			mainPanel(
				dataTableOutput("latest")
			)
		),
    		tabPanel("Summary",
			sidebarPanel(
				#div("Lite version is under development, this functionality is very slow atm", style = "color:red"),
				textInput("char2", label = h4("Character shortName"), value = "")
			),
			mainPanel(
				htmlOutput("summary")
			)
    		)
    	)
))

