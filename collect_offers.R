# Set a proper working directory
setwd("D:\\CKR\\")
# Read overall item data from November 28; this is mainly to obtain a list of potential item IDs
AgoraNov28 <- read.table("AgoraNov28.txt", sep="\t", header=TRUE, quote="\"", comment.char="¤")
ids <- rev(AgoraNov28[,"ID"])

# Package 'jsonlite' is required for reading in the JSON format API output
library(jsonlite)
# Create an empty vector of items list
items <- vector("list", length=length(ids))
names(items) <- ids

starttime <- Sys.time()
for(item in ids){
	print(item)
	f <- url(paste("https://cryptokingdom.me/api/getStateForItem/", item, sep=""), method="wininet")
	items[[item]] <- try(fromJSON(readLines(f)))
	close(f)
	Sys.sleep(0.2) # Sleep for a 20% second so we don't overburden the API
}
endtime <- Sys.time()

save(items, file=paste("CK_BUYSELL_", gsub(":", "-", gsub(" ", "_", x=Sys.time())), ".RData", sep=""))





