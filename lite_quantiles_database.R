setwd("D:\\CKJester\\DB\\")
dbname <- "DB_2016-01-10"
load(paste(dbname,".RData", sep=""))

items <- vector("list", length=length(unique(db[,"itemID"])))
names(items) <- unique(db[,"itemID"])

# For weighted quantiles
library(Hmisc)
# Compute quantiles by precision of 0.01
p <- seq(from=0, to=1, by=0.01)

for(x in names(items)){
	print(x)
	sub.buy <- subset(db, db$bid==1 & db$itemID == x)
	sub.sell <- subset(db, db$bid==0 & db$itemID == x)
	wtd.buy <- NA
	if(nrow(sub.buy)>=1){
		wtd.buy <- do.call("rbind", by(sub.buy, INDICES=sub.buy[,"Time"], FUN=function(z){
			wtd.quantile(x=z[,"price"], weight=z[,"amount"], probs=p)
		}))
	}
	wtd.sell <- NA
	if(nrow(sub.sell)>=1){
		wtd.sell <- do.call("rbind", by(sub.sell, INDICES=sub.sell[,"Time"], FUN=function(z){
			wtd.quantile(x=z[,"price"], weight=z[,"amount"], probs=p)
		}))
	}
	items[[x]] <- list(buyqs = wtd.buy, sellqs = wtd.sell)
}

save(items, file=paste(dbname,"_LITE.RData", sep=""))









quantileplot <- function(
	name="HM02B100",
	items,
	x = items[[name]], 
	yrange=range(rbind(x[[1]], x[[2]])), 
	xrange=range(c(
		as.POSIXlt(rownames(x[[1]])),
		as.POSIXlt(rownames(x[[2]]))
	)),
	npoly=100,
	palet.sell = colorRampPalette(c("blue","cyan"))(101), # p was 101 different quantiles
	palet.buy = colorRampPalette(c("orange","red"))(101), # p was 101 different quantiles
	logy = TRUE,
	lines = TRUE,
	meancurve = TRUE,
	...
){
	#x[[1]] == buy offers
	#x[[2]] == sell offers
	#print(x)
	#print(yrange)
	#print(xrange)
	#l <- matrix(c(1,2,3,4,4,4,5,5,5), nrow=3, byrow=T)
	#layout(l, heights=c(0.1,0.7,0.2), widths=c(1,1,1))
	l <- matrix(c(1,2,3,4,4,4), ncol=3, byrow=T)
	layout(l, heights=c(0.1,0.7), widths=c(2,1,1))
	
	# Annotations section
	par(mar=c(0.5,0.5,0.5,0.5), las=2)
	plot.new()
	plot.window(xlim=c(0,1), ylim=c(0,1))
	#if(input$scatteroffers) legend("topleft", bty="n", col=c(palet.buy[1], palet.sell[1]), pch=16, legend=c("Buy offers", "Sell offers"), horiz=T, cex=1.0)
	if(meancurve) legend("bottomleft", bty="n", col="black", legend="Mean lowest sell / highest buy", lty=2, lwd=2)

	par(mar=c(0.5,4,0.5,0.5), las=2)
	plot.new()
	plot.window(xlim=c(0,1), ylim=c(0,1))
	ax2 <- axis(2, tick=F, labels=F)
	ax2 <- seq(from=ax2[1], to=ax2[length(ax2)], length.out=5)
	axis(2, at=ax2, labels=ax2)
	abline(h=p, col=palet.sell, lwd=2)
	title(ylab="Sell quant.")

	par(mar=c(0.5,4,0.5,0.5), las=2)
	plot.new()
	plot.window(xlim=c(0,1), ylim=c(0,1))
	ax2 <- axis(2, tick=F, labels=F)
	ax2 <- seq(from=ax2[1], to=ax2[length(ax2)], length.out=5)
	axis(2, at=ax2, labels=ax2)
	abline(h=p, col=palet.buy, lwd=2)
	title(ylab="Buy quant.")
	
	
	# Quantile plots
	par(las=2, mar=c(6,8,3,1))
	plot.new()
	plot.window(xlim=as.numeric(xrange), ylim=yrange, log=ifelse(logy, "y", ""))
	#axis(1); 
	xseq <- seq(xrange[1], xrange[2], by="day")
	#axis.POSIXct(1, at=xseq[round(seq(from=1, to=length(xseq), length.out=20),0)], format="%Y-%m-%d at %H h")
	axis.POSIXct(1, at=xseq[round(seq(from=1, to=length(xseq), length.out=20),0)], format="%Y-%m-%d")
	#axis(2); box()
	ax2 <- axis(2, tick=F, labels=F)
	axis(2, at=ax2, labels=format(ax2, big.mark=",",scientific=FALSE))
	title(main=name, ylab=paste("Offer price", ifelse(logy, "(log)", ""), "\n\n\n"))
	box()
	# Sell quantiles
	invisible(lapply(1:(ncol(x[[1]])-1), FUN=function(z){
		# Create polygons
		xvec <- c(as.numeric(as.POSIXct(rownames(x[[1]][,z,drop=F]))), rev(as.numeric(as.POSIXct(rownames(x[[1]][,z+1,drop=F])))))
		yvec <- c(x[[1]][,z], rev(x[[1]][,z+1]))
		polygon(x=xvec, y=yvec, col=palet.buy[z], border=palet.buy[z])	
	}))
	# Buy quantiles
	invisible(lapply(1:(ncol(x[[1]])-1), FUN=function(z){
		# Create polygons
		xvec <- c(as.numeric(as.POSIXct(rownames(x[[2]][,z,drop=F]))), rev(as.numeric(as.POSIXct(rownames(x[[2]][,z+1,drop=F])))))
		yvec <- c(x[[2]][,z], rev(x[[2]][,z+1]))
		polygon(x=xvec, y=yvec, col=palet.sell[z], border=palet.sell[z])	
	}))
	# Mean of lowest sell / highest buy
	if((!is.null(x[[1]]) & nrow(x[[1]])>0) & (!is.null(x[[2]]) & nrow(x[[2]])>0) & meancurve) { #input$meancurve){
		times <- unique(rownames(x[[1]]), rownames(x[[2]]))
		means <- unlist(lapply(times, FUN=function(z){
			mean(c(
				max(x[[1]][z,]), min(x[[2]][z,])
			), na.rm=T)
		}))
		#print(times)
		#print(means)
		points(as.numeric(as.POSIXct(times)), means, type="l", lty=2, lwd=2, col="black")
	}
	if(lines){
		abline(h=ax2, col="grey")
	}
}


library("Cairo")

#png("Dec29_HM02B100.png", width=700, height=700, type="cairo-png", antialias="gray")
CairoPDF("DEC29_HM02B100.pdf", width=6, height=6)
quantileplot(name="HM02B100", items=items, yrange=c(80e6, 120e6), logy=FALSE)
dev.off()

CairoPDF("DEC29_CON.pdf", width=6, height=6)
quantileplot(name="CON", items=items, yrange=c(14e6, 17e6), logy=FALSE)
dev.off()

CairoPDF("DEC29_CAN.pdf", width=6, height=6)
quantileplot(name="CAN", items=items, yrange=c(1000, 20000))
dev.off()

CairoPDF("DEC29_BEER.pdf", width=6, height=6)
#png("Dec29_BEER.png", width=700, height=700, type="cairo-png")
quantileplot(name="BEER", items=items, yrange=c(3000, 10000), logy=FALSE)
dev.off()


